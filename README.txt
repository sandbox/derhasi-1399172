
This module is a helper module for bringing mobile_tools and securepages together,
especially when modules like GlobalRedirect are enabled.

* http://drupal.org/project/securepages
* http://drupal.org/project/mobile_tools
* http://drupal.org/project/globalredirect

See blogpost on http://www.undpaul.de/blog/2012/01/10/taming-mobile-and-secure-pages
for more information on that.

Files
=====

- default.settings.inc
  This is the settings.php snippet, which does that configuration work for mobile
  tools and SecurePages on the fly on each page request.

- mobiletools_securepages_helper.module
  The module itself only provides an additional form submission handling, that
  will delete the URL configuration for mobile tools and securepages from the
  database, so it can be dynamically set in the settings.php .