<?php

/**
 * @file
 *   configuration to copy to settings.php to make securepages and mobile_tools work together.
 *
 *   This snippet works for secure pages, that only differ from the non-secure
 *   one, in using https:// instead of http://. For other configurations you
 *   might need to change the snippet.
 */

// Let mobile tools work with secure pages.
// We have to set the base_url too, so e.g. globalredirect will not force a
// wrong redirect.

$desktop_url = 'www.example.com';
$mobile_url = 'm.example.com';

$secure = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on');

// On mobile requests, mobile path provide the secure settings.
if ($_SERVER['HTTP_HOST'] == $mobile_url) {
  $conf['securepages_basepath'] = "http://$mobile_url";
  $conf['securepages_basepath_ssl'] = "https://$mobile_url";
  $base_url = ($secure) ? "https://$mobile_url" : "http://$mobile_url";
}
else {
  $conf['securepages_basepath'] = "http://$desktop_url";
  $conf['securepages_basepath_ssl'] = "https://$desktop_url";
  $base_url = ($secure) ? "https://$desktop_url" : "http://$desktop_url";
}

// On secure pages, mobile and desktop urls are https urls.
if ($secure) {
  $conf['mobile_tools_mobile_url'] = "https://$mobile_url";
  $conf['mobile_tools_desktop_url'] = "https://$desktop_url";
}
else {
  $conf['mobile_tools_mobile_url'] = "http://$mobile_url";
  $conf['mobile_tools_desktop_url'] = "http://$desktop_url";
}
